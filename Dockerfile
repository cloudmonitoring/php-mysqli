FROM php:7.2-cli
RUN docker-php-ext-install mysqli
#COPY  ./webcontent /usr/src/myapp
WORKDIR /usr/src/myapp
CMD [ "php", "./get-zabbix-usage.php" ]

